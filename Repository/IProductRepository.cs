﻿using Microsoft.AspNetCore.JsonPatch;
using ProductAPI.Model;

namespace ProductAPI.Repository
{
    public interface IProductRepository
    {
        List<Product> GetAllProducts();
        bool AddProduct(Product product);
        bool DeleteProduct(int id);
        bool UpdateProduct(int id, JsonPatchDocument product);

    }
}
