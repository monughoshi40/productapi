﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using ProductAPI.Model;
using ProductAPI.Repository;


namespace ProductAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        readonly IProductRepository _productRepository;
        //constructor
        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;

        }


        [Route("getAllProducts")]
        [HttpGet]
        public ActionResult GetAllProducts()
        {
            _productRepository.GetAllProducts();
            List<Product> allProducts = _productRepository.GetAllProducts();
            return Ok(allProducts);
        }

        //100 ----> Informational responces
        //200 ----> Successful responces
        //300 ----> redirectional responce
        //400 ----> client side responces
        //500 -----> server side error



        [Route("addProduct")]
        [HttpPost]
        public ActionResult AddProduct(Product product)
        {
            bool addProductStatus = _productRepository.AddProduct(product);
            return Created("api/Created", addProductStatus);
        }

        [Route("DeleteProduct/{id:int}")]
        [HttpDelete]

        public ActionResult DeleteProduct(int id)
        {
            bool deleteProductStatus = _productRepository.DeleteProduct(id);
            return Ok(deleteProductStatus);


        }

        [Route("UpdateProduct /{id: int}")]
        [HttpPatch]
        public ActionResult UpdateProduct(int id, JsonPatchDocument product)
        {
            bool editProductstatus = _productRepository.UpdateProduct(id, product);
           return Ok(editProductstatus);



        }









    }
}
